#!/bin/bash -e

. "${DDTP_CONFIG_FILE}"

# Regular vacuum to cut disk usage
psql "${DDTP_PSQL_CONNECTION_STRING}" -c "VACUUM active_tb,calculated_stamps,collection_milestone_tb,ddtss,ddtss_old,description_milestone_tb,description_tag_tb,description_tb,done_packages2,owner_tb,package_version_tb,packages_tb,part_description_tb,part_tb,ppart_tb,statistic_tb,suggestion_tb,translation_tb,version_tb"