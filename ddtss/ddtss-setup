#!/usr/bin/perl -w

# This script allows you to configure various parameters. It can be used to
# create the initial database, or to update the parameters later on.

use strict;
use Pg_BDB;

my @langs = qw(da de el es fi fr gl hu id it ja km_KH kn ko nb nl pl sv zh_CN pt pt_AO pt_BR eo zh_TW ca cs uk vi ml ru sk sr bg);
my @disabled_langs = qw(go);

my $db = Pg_BDB->open_write();
# Language codes available for translations
$db->put('langs', join(",", @langs));
$db->put("langs/disabled", join(",",@disabled_langs));

# Set default values for all languages 
for my $lang (@langs)
{
# Minimum number of untranslated descriptions to have at any time
  $db->put("$lang/config/minuntranslated", '1');
# Number of reviewers before sending, can be zero
  $db->put("$lang/config/numreviewers", '3');
# Do we require translators to be registered?
  $db->put("$lang/config/requirelogin", '1');
}

# Special value for nl
$db->put('ca/config/minuntranslated', 2);
$db->put('cs/config/minuntranslated', 2);
$db->put('cs/config/numreviewers', '2');
$db->put('da/config/minuntranslated', '3');
$db->put('da/config/numreviewers', '2');
$db->put('da/config/requirelogin', '0');
$db->put('de/config/minuntranslated', '4');
$db->put('de/config/requirelogin', '0');
$db->put('eo/config/numreviewers', '1');
$db->put('es/config/minuntranslated', '50');
$db->put('es/config/numreviewers', '2');
$db->put('fi/config/minuntranslated', '2');
$db->put('fr/config/minuntranslated', '4');
$db->put('fr/config/numreviewers', '2');
$db->put('it/config/minuntranslated', '3');
$db->put('ja/config/minuntranslated', '1');
$db->put('km_KH/config/minuntranslated', '1');
$db->put('kn/config/minuntranslated', '1');
$db->put('nl/config/minuntranslated', '3');
$db->put('nl/config/numreviewers', '2');
$db->put('pl/config/minuntranslated', '2');
$db->put('pl/config/requirelogin', '0');
$db->put('pt_BR/config/minuntranslated', '3');
$db->put('pt_PT/config/minuntranslated', '3');
$db->put('pt_PT/config/numreviewers', '2');
$db->put('ru/config/numreviewers', '2');
$db->put('sk/config/numreviewers', '1');
$db->put('uk/config/requirelogin', '0');
$db->put('vi/config/numreviewers', '1');
$db->put('vi/config/requirelogin', '1');
$db->put('zh_CN/config/minuntranslated', 2);
$db->put('zh_TW/config/minuntranslated', 2);
$db->put('zh_TW/config/numreviewers', 2);

# Email address of DDTS
$db->put('config/serveremail', '<pdesc@ddtp.debian.org>');
# This email (if present) will be BCC'd for any outgoing email
$db->put('config/debugemail', '<test@kleptog.org>');
# Your email address. The DDTS will send its replies to this address so 
# you have to be able to feed those emails to ddtss-process
$db->put('config/clientemail', 'Martijns DDTSS <ddtss@kleptog.org>');

$db->close();

