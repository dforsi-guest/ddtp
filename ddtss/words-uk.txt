2d	двовимірний
3d	тривимірний
application	програма, застосунок
backup	резервування, резервна копія
binary	двійковий
binding	привʼязка
bug	недолік, вада
bundle	вʼязка, клунок
catalog	тека, каталог
cli	інтерфейс командного рядка
collection	набір, зібрання, збірка
common-files	спільні файли
commonfiles	спільні файли
compressed	стиснений
configuration	налаштування, склад обладнання
cross-platform	багатоплатформовий, мультиплатформовий
crossplatform	багатоплатформовий, мультиплатформовий
custom	власний, нетиповий
customizable	налаштовуваний
daemon	демон, фонова служба
debug	зневадження, налагодження
debugging-symbols	символи налагодження
debuggingsymbols	символи налагодження
default	типовий, за замовчуванням, стандартний
desktop	стільниця
directory	каталог
dispatch	передавати керування, координувати, відправити, розпорядження
display	відображати
dummy	фіктивний
dump	аварійне знімання, скинути
enhanced	покращений, вдосконалений
extension	розширення
folder	тека
framework	(інфра)структура, каркас
frontend	зовнішній інтерфейс
game	гра
getting started	початок роботи
gui	ГІК, ГІ
implementation	реалізація
lightweight	легкий
log	журнал
logging	журналювання
manage	керувати, управляти
meta-package	збірний пакунок
metapackage	збірний пакунок
multicast	групова/широкомовна трансляція/передача
package	пакунок
packet	пакет
parse	розбір, аналіз
parser	аналізатор, розбір
patch	латка
plugin	втулка
pool	злиття, схованка
powerful	потужний
privilege	привілей
provide	надавати, забезпечувати
pull	тягти
purge	очистити
puzzle	головоломка, загадка, ребус
queue	черга
raw	необроблений, неопрацьований
remote	віддалений, дистанційний
render	промальовка, візуалізація, відтворення
repository	репозиторій(-ю)
resolution	роздільність, роздільна здатність
resolve	розвʼязувати
revision-control	(система) керування версіями
revisioncontrol	(система) керування версіями
runtime	виконавчий
screen	екран
script	скрипт
service	сервіс, служба
shared	колективний
shell	оболонка
shooter	стрілянка
source	сирці, вихідний код, джерело
tag	мітка
tool	засіб, інструмент
transitional	перехідний
useful	зручний
utility	утиліта
widget	візуальний компонент, віджет
wraper	обгортка