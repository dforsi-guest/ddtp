accessibility	dostępność
account	konto
acknowledgement	podziękowanie, potwierdzenie
applet	aplet
application	aplikacja
app	aplikacja
apps	aplikacje
application programming interface	interfejs programowania aplikacji (API)
artwork	prace graficzne
authentication	uwierzytelnianie
authorization	autoryzacja
backslash	odwrotny ukośnik (opisując znak "\"), poprzeczny ukośnik
bug	błąd
bug report	zgłoszenie błędu
bug tracking system	system śledzenia błędów 
bylaw	zarządzanie, przepis
back end	backend
binaries	pliki binarne
boot	rozruch, ładowanie
boot loader	program rozruchowy
buffer overflow	przepełnienie bufora
callback	wywołanie zwrotne (ang. callback
caution	przestroga
changelog	dziennik zmian (changelog)
character set	zestaw znaków
charset	zestaw znaków
code page	strona kodowa
command	polecenie
command history	historia poleceń
command line	wiersz poleceń
compile	kompilować
compiler	kompilator
completion	dopełnianie
constitution	konstytucja, statut
control center	centrum sterowania
cross compile	kompilacja skrośna
cross platform	wieloplatformowy
curses	curses
database	baza danych
daemon	demon
Debian Policy	Polityka Debiana
debugger	debugger
debug	debugować, analizować, testować, odpluskiwać
debugging	debugowanie
delete	usuń, skasuj
desktop	pulpit
desktop enviroment	środowisko graficzne
developer	deweloper, opiekun (jeżeli chodzi o pakiet/program/itp)
display	manager, menedżer logowania
distribution	dystrybucja
dummy package	pakiet atrapa
e mail	poczta elektroniczna (w przypadku mowy o całości), adres poczty elektronicznej, list (elektroniczny)
encoding	kodowanie źródłowe
event	zdarzenie
exit	zakończ, wyjdź
exploit	exploit
firmware	firmware
font	czcionka
frame buffer	bufor ramki
free software fundation	Fundacja Wolnego Oprogramowania (przy pierwszym tłumaczeniu dobrze jest dodać w nawiasie skrót FSF)
front end	nakładka, interfejs
full featured	w pełni funkcjonalny
fuzzy search	wyszukiwanie rozmyte
gateway	brama 
graph	wykres
general resolution	uchwała, ustawa
general purpose	uniwersalny
guide	przewodnik
handbook	podręcznik
hardware	sprzęt
hard link	dowiązanie, dowiązanie (sztywne)
header file	plik nagłówkowy
highly configurable	wysoce konfigurowalny
host	komputer
important	ważne
include	zawierać, włączać, obejmować, uwzględniać
input method	system wprowadzania (np. znaków)
internationalized	umiędzynarodowiony 
kernel	jądro, jajko, kernel
kill	usuń (w stosunku do procesów, `kill -9 pid`), zniszcz (okno WM), zabij
library	biblioteka
lightweight	lekki
line	wiersz (w edytorze), linia (geometria)
link	dowiązanie (pliki utworzone przy pomocy `ln`), odsyłacz (element strony www), łączyć (przy kompilacji)
log	log
log file	log
login	login, nazwa użytkownika
lookup	podgląd
mail delivery agent	pocztowy agent doręczeniowy, MDA
mail user agent	program pocztowy, MUA
mail transport agent	serwer pocztowy, MTA
mailing list	lista dyskusyjna, lista mailingowa
man page	strona podręcznika man, strona podręcznika systemowego
memory dump	zrzut pamięci
mirror	serwer lustrzany, mirror
multi lingual	wielojęzyczny
native	natywny
newsreader	czytnik grup dyskusyjnych
note	zauważ (np. w konstrukcji 'note that...'), uwaga, notatka
notification area	obszar powiadamiania
offset	offset
package	pakiet
packages	pakiety
plug in	wtyczka, plugin, moduł
portable	przenośny
prompt	znak zachęty
proxy	proxy, pośrednik (serwer pośredniczący w HTTP, FTP etc.)
quit	zakończ, wyjdź
reboot	ponowne uruchomienie, reboot
release critical bug	błąd krytyczny dla wydania
replace	zastąp, zamień
road map	plan (plany na przyszłość), harmonogram (prac), mapa drogowa
root	partycja główna (/), administrator
RSS aggregator	czytnik RSS
runtime	uruchomieniowy
section	rozdział, część, sekcja
security advisory	informacje nt. bezpieczeństwa
security policy	polityka bezpieczeństwa
server side	działający/wykonywany po stronie serwera
shared library	biblioteka współdzielona
shell	powłoka, shell
shell prompt	znak zachęty powłoki
slash	ukośnik
snoop	szpiegować, podglądać
snooping	szpiegowanie, podglądanie
sound server	serwer dźwięku
spell checker	program do sprawdzania pisowni
spreadsheet	arkusz kalkulacyjny
spoof	fałszować
spoofing	fałszowanie (np. IP spoofing - fałszowanie adresu)
stand alone	samodzielny, niezależny
status bar	pasek statusu, linia statusu, wiersz stanu
stdin	standardowe wejście 
stdout	standardowe wyjście
string	napis
substitution	zamiennik
substitute	zamiennik
support	obsługa, wsparcie 
suspend	wstrzymanie 
suspend to RAM	wstrzymanie do pamięci
suspend to disk	wstrzymanie do dysku)
swap	przestrzeń wymiany, obszar wymiany, plik wymiany (swapfile)
swap file	plik wymiany
swiss army knife	uniwersalne narzędzie
symlink	dowiązanie symboliczne, link symboliczny
system tray	zasobnik systemowy
systray	zasobnik systemowy
tag	znacznik (w systemie śledzenia błędów)
theme	motyw
tip	wskazówka
toolbox	przybornik
toolchain	zestaw narzędzi programistycznych (tzw. toolchain)
toolkit	zestaw narzędzi
total	całkowity (przymiotnik),  (rzeczownik)
touchpad	touchpad [nie tłumaczymy]
trashcan	kosz, śmietnik
tutor		nauczyciel
tutorial	samouczek
unices	uniksy
update	aktualizacja, uaktualnienie, update
upgrade	aktualizacja, uaktualnienie, upgrade
upstream developer	zewnętrzny deweloper/autor, zewnętrzny opiekun
utility	narzędzie
vanilla	czyste, niezmodyfikowane, standardowe, domyślne
warning	ostrzeżenie
widget	widżet
workstation	stacja robocza
wrapper	opakowanie
wrapper class	klasa opakowująca
Debian Free Software Guidelines	Wytyczne Debiana dotyczące Wolnego Oprogramowania, Wytyczne Debiana (przy powtorzeniach)